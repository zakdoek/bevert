/**
 * Tag the html element with responsive classes on screen resize
 */
define(function() {

    "use strict";

    var TIERS = {

        "xs": {
            min: true,
            max: 767
        },

        "sm": {
            min: 768,
            max: 991
        },

        "md": {
            min: 992,
            max: 1199
        },

        "lg": {
            min: 1200,
            max: true
        }

    },
    
    $ = jQuery;

    $( document ).ready(function() {

        var $html = $( "html" ),
            $win = $( window );

        function onResize() {

            var key, toAdd,
                toRemove = "",
                w = $win.width(),
                aspect = w / $win.height();

            // Set the responsive tiers
            for( key in TIERS ) {
                var prop = TIERS[ key ];

                // Todo, large does not get set
                if (  ( prop.min === true || prop.min <= w ) &&
                      ( prop.max === true || prop.max >= w )  ) {
                    toAdd = key;
                } else {
                    toRemove += key + " ";
                }
            }

            $html.addClass( toAdd ).removeClass( toRemove );

            // Set screen format type (portrait or landscape)
            if ( aspect <= 1 ) {
                $html.addClass( "portrait" ).removeClass( "landscape" );
            } else {
                $html.addClass( "landscape" ).removeClass( "portrait" );
            }
        }

        $win.resize( onResize );
        onResize();

    });

});
