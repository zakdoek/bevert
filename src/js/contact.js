define(function() {

    "use strict";

    require( "app/gmaps" );

    var $ = jQuery,
        $win = $( window ),
        $doc = $( document );

    $doc.ready(function(){

        // Test for vh support
        var $contactMap = $( "#contact-map" );

        if ( !$contactMap.height() ) {

            // No vh support, fix
            var onResize = function() {
                $contactMap.height( $win.height() - 100 );
            };
            $win.resize( onResize );
            onResize();

        }

    });

    $.gmapAPILoaded(function() {
        var $map = $( "#contact-map" ),
            $navigation = $( "#main-navigation" ),
            map = $map.gmap({
            config: {
                zoom: 14,
                center: {
                    lat: 51.169472,
                    lng: 3.251194
                },
                extra: {
                    styles: [
                        {
                            stylers: [
                                { hue: "#b26300" },
                                { gamma: 0.5 }
                            ]
                        }
                    ],
                    disableDefaultUI: true,
                    disableDoubleClickZoom: true,
                    mapTypeControl: false,
                    scrollwheel: false,
                    draggable: false
                }
            },
            markers: [
                {
                    position: {
                        lat: 51.169472,
                        lng: 3.251194
                    },
                    title: "4K Interieur",
                    extra: {
                        icon: window.ASSETS + "img/bitmap/marker.png"
                    }
                }
            ]
        }).gmap( "getMap" );

        var mapEnabled = false,
            $toggleButton = $( "#contact-activate-toggle" );

        $toggleButton.click(function() {
            mapEnabled = !mapEnabled;
            var options = {
                disableDefaultUI: !mapEnabled,
                disableDoubleClickZoom: !mapEnabled,
                scrollwheel: mapEnabled,
                draggable: mapEnabled,
                mapTypeControl: mapEnabled
            };
            map.map.setOptions( options );

            if ( mapEnabled ) {
                // Change button text
                $toggleButton.text( "Deactiveer kaart" );

                // Scroll map in full view
                $( "html,body" ).animate({
                    scrollTop: $map.offset().top - $navigation.height()
                }, 500);
            } else {
                // Change button text
                $toggleButton.text( "Activeer kaart" );

                // Close possible street view
                map.map.getStreetView().setVisible( false );
            }
        });
    });

});
