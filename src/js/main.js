define(function() {

    "use strict";

    require( "app/responsive-class" );

    // Script the naviation
    require( "app/navigation" );

    // Script the contact page
    require( "app/contact" );

});
