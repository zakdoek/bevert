define(function() {

    "use strict";

    var $ = jQuery,
        $win = $( window ),
        $body = $( "body" ),
        $htmlBody = $( "html,body" ),
        $navigation = $( "#main-navigation" ),
        $home = $( "#home" );

    // Update the active page in navigation
    $body.scrollspy({
        target: "#main-navigation"
    });

    // Let the navigation bar stick to top
    $navigation.affix({
        offset: {
            top: function() {
                return $win.height();
            }
        }
    });


    function scanForScroll( $element ) {

        $( "a[href*=#]:not([href=#])", $element ).click(function() {

            var current = location.pathname.replace( /^\//,"" );
            var targetPath = this.pathname.replace( /^\//,"" );
            if (  current ===  targetPath &&
                  location.hostname === this.hostname ) {
                var target = $( this.hash );
                if ( !target.length ) {
                    target = $( "[name=" + this.hash.slice( 1 ) +"]" );
                }
                if ( target.length ) {
                    $htmlBody.animate({
                        scrollTop: target.offset().top
                    }, 1000 );
                    return false;
                }
            }
        });
    }
    scanForScroll( $navigation );
    scanForScroll( $home );

});
