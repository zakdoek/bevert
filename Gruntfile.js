module.exports = function( grunt ) {

  "use strict";

  // Load tasks
  require( "load-grunt-tasks" )( grunt );
  grunt.loadNpmTasks( "assemble" );

  // Time grunt tasks execution
  require( "time-grunt" )( grunt );

  grunt.initConfig({

    // Some general config
    bevert: {
      src: "src/",

      development: "build/development/",

      release: "build/release/",

      bower: "bower_components/",

      bootstrapJs: "bootstrap-sass-official/assets/javascripts/" +
                   "bootstrap/",

      cache: "grunt-tmp/"

    },

    // Watch
    watch: {

      compass: {
        files: "<%= bevert.src %>scss/**/*.scss",
        tasks: [ "compass:development" ]
      },

      javascripts: {
        files: "<%= bevert.src %>js/**/*.js",
        tasks: [ "jshint:src", "requirejs:development" ]
      },

      bitmaps: {
        files: "<%= bevert.src %>img/bitmap/**/*.{jpg,png}",
        tasks: [ "imagemin:development" ]
      },

      svg: {
        files: "<%= bevert.src %>img/svg/**/*.svg",
        tasks: [ "grunticon:development" ]
      },

      html: {
        files: "<%= bevert.src %>html/**/*.hbs",
        tasks: [ "assemble:development" ]
      },

      gruntfile: {
        files: "Gruntfile.js",
        tasks: [
          "jshint:gruntfile",
          "copy:development",
          "compass:development",
          "grunticon:development",
          "requirejs:development"
        ]
      }

    },

    // Clean task.
    clean: {
      development: {
        src: [ "<%= bevert.development %>", "<%= bevert.cache %>" ]
      },
      release: {
        src: [ "<%= bevert.release %>", "<%= bevert.cache %>" ]
      }
    },

    // Copy task.
    copy: {
      development: {
        files: [

          // HtmlShiv
          {
            src: "<%= bevert.bower %>html5shiv/dist/html5shiv.js",
            dest: "<%= bevert.development %>js/html5shiv.js"
          },

          // Respond
          {
            src: "<%= bevert.bower %>respond/dest/respond.min.js",
            dest: "<%= bevert.development %>js/respond.js"
          },

          // Bootstrap glyphicons
          {
            expand: true,
            cwd: "<%= bevert.bower %>bootstrap-sass-official/assets/" +
                 "fonts/bootstrap/",
            src: "**/*",
            dest: "<%= bevert.development %>fonts/bootstrap/"
          },

          // Fonts
          {
            expand: true,
            cwd: "<%= bevert.src %>fonts/",
            src: "**/*",
            dest: "<%= bevert.development %>fonts/"
          }

        ]
      },
      release: {
        files: [

          // HtmlShiv
          {
            src: "<%= bevert.bower %>html5shiv/dist/html5shiv.js",
            dest: "<%= bevert.release %>js/html5shiv.js"
          },

          // Respond
          {
            src: "<%= bevert.bower %>respond/dest/respond.min.js",
            dest: "<%= bevert.release %>js/respond.js"
          },

          // Bootstrap glyphicons
          {
            expand: true,
            cwd: "<%= bevert.bower %>bootstrap-sass-official/assets/" +
                 "fonts/bootstrap/",
            src: "**/*",
            dest: "<%= bevert.release %>fonts/bootstrap/"
          },

          // Fonts
          {
            expand: true,
            cwd: "<%= bevert.src %>fonts/",
            src: "**/*",
            dest: "<%= bevert.release %>fonts/"
          }

        ]
      }
    },

    // Compass task.
    compass: {
      options: {
        sassDir: "<%= bevert.src %>scss/",
        relativeAssets: true,
        importPath: [
          "<%= bevert.bower %>bootstrap-sass-official/assets/" +
          "stylesheets/"
        ]
      },
      development: {
        options: {
          cssDir: "<%= bevert.development %>css/",
          fontsDir: "<%= bevert.development %>fonts/",
          imagesDir: "<%= bevert.development %>img/bitmap/",
          outputStyle: "nested"
        }
      },
      release: {
        options: {
          cssDir: "<%= bevert.release %>css/",
          fontsDir: "<%= bevert.release %>fonts/",
          imagesDir: "<%= bevert.release %>img/bitmap/",
          outputStyle: "compressed"
        }
      }
    },

    // Requirejs task
    requirejs: {

      // General options
      options: {
        // Set the base url to the bower repo for convenience
        baseUrl: "<%= bevert.bower %>",
        name: "almond/almond",
        wrap: true,
        paths: {
          "app": "../<%= bevert.src %>js/"
        },
        include: [
          // Include jquery
          "jquery/dist/jquery.js",

          // Include the bootstrap libraries
          "<%= bevert.bootstrapJs %>affix.js",
          "<%= bevert.bootstrapJs %>alert.js",
          "<%= bevert.bootstrapJs %>button.js",
          "<%= bevert.bootstrapJs %>carousel.js",
          "<%= bevert.bootstrapJs %>collapse.js",
          "<%= bevert.bootstrapJs %>dropdown.js",
          "<%= bevert.bootstrapJs %>tab.js",
          "<%= bevert.bootstrapJs %>transition.js",
          "<%= bevert.bootstrapJs %>scrollspy.js",
          "<%= bevert.bootstrapJs %>modal.js",
          "<%= bevert.bootstrapJs %>tooltip.js",
          "<%= bevert.bootstrapJs %>popover.js",

          // Include tha main app as last
          "app/main"
        ],

        // Require the last app
        insertRequire: [ "app/main" ]

      },

      // Development specific options
      development: {
        options: {
          out: "<%= bevert.development %>js/script.min.js",
          optimize: "none"
        }
      },

      // Release build specifics
      release: {
        options: {
          out: "<%= bevert.release %>js/script.min.js",
          optimize: "uglify2",
          preserveLicenseComments: false
        }
      }
    },

    // Minify images
    imagemin: {

      development: {
        options: {
          optimizationLevel: 0
        },

        files: [
          {
            expand: true,
            cwd: "<%= bevert.src %>img/bitmap/",
            src: "**/*.{jpg,png}",
            dest: "<%= bevert.development %>img/bitmap/"
          }
        ]
      },

      release: {
        files: [
          {
            expand: true,
            cwd: "<%= bevert.src %>img/bitmap/",
            src: "**/*.{jpg,png}",
            dest: "<%= bevert.release %>img/bitmap/"
          }
        ]
      }

    },

    // Minify and build svg images
    grunticon: {

      // General options
      options: {
        datasvgcss: "svg.css",
        datapngcss: "png.css",
        urlpngcss: "ref.css",
        customselectors: {
          "logo": [ "#home" ],
          "logo-xs": [ ".xs #home" ],
          "scroll-down": [ "#home .next-page" ],
          "scroll-down-xs-landscape": [ ".xs.landscape #home .next-page" ],
          "brand": [ ".navbar-brand" ],
          "underline": [ "#main-navigation .active a" ]
        }
      },
      development: {
        files: [{
          expand: true,
          cwd: "<%= bevert.src %>img/svg/",
          src: "*.svg",
          dest: "<%= bevert.development %>img/svg/"
        }]
      },
      release: {
        files: [{
          expand: true,
          cwd: "<%= bevert.src %>img/svg/",
          src: "*.svg",
          dest: "<%= bevert.release %>img/svg/"
        }]
      }
    },

    // Connect task
    connect: {
      develop: {
        options: {
          port: 9000,
          hostname: "*",
          base: "<%= bevert.development %>"
        }
      }
    },

    // Jshint task
    jshint: {
      options: {
        jshintrc: true
      },

      src: {
        src: "<%= bevert.src %>/js/**/*.js"
      },

      gruntfile: {
        src: "Gruntfile.js"
      }
    },

    // Assemble task
    assemble: {

      options: {
        flatten: true,
        layout: "<%= bevert.src %>html/layouts/default.hbs",
        partials: [ "<%= bevert.src %>html/includes/*.hbs" ],
        data: "<%= bevert.src %>data/**/*.{json,yml}"
      },

      development: {
        options: {
          assets: "<%= bevert.development %>"
        },

        src: [ "<%= bevert.src %>/html/pages/**/*.hbs" ],
        dest: "<%= bevert.development %>"
      },

      release: {
        options: {
          assets: "<%= bevert.release %>"
        },
        src: [ "<%= bevert.src %>html/pages/**/*.hbs" ],
        dest: "<%= bevert.release %>"
      }

    },

    // Htmlmin task
    htmlmin: {

      release: {
        options: {
          removeComments: true,
          collapseWhitespace: true
        },
        expand: true,
        cwd: "<%= bevert.release %>",
        src: [ "**/*.html" ],
        dest: "<%= bevert.release %>"
      }

    }
  });

  grunt.registerTask( "development", [
    "jshint:gruntfile",
    "clean:development",
    "copy:development",
    "imagemin:development",
    "grunticon:development",
    "compass:development",
    "jshint:src",
    "requirejs:development",
    "assemble:development"
  ]);

  grunt.registerTask( "release", [
    "jshint:gruntfile",
    "clean:release",
    "copy:release",
    "imagemin:release",
    "grunticon:release",
    "compass:release",
    "jshint:src",
    "requirejs:release",
    "assemble:release",
    "htmlmin:release"
  ]);

};
